// actualisation toutes les secondes
setInterval(function () {
    //récupération de l'heure actuelle
    let curDate = new Date();
    let curHour = curDate.getHours()

    let angleH = curHour % 12 / 12 * 360
    let angleM = curDate.getMinutes() / 60 * 360
    let angleS = curDate.getSeconds() / 60 * 360

    // impression de l'heure actuelle
    document.querySelector('#currentClockBloc .hour').style.transform = 'rotate(' + angleH + 'deg)';
    // affichage des minutes dans toutes les horloges
    for (const horloge of document.querySelectorAll('.minutes')) {
        horloge.style.transform = 'rotate(' + angleM + 'deg)';
    }
    // affichage des secondes dans toutes les horloges
    for (const horloge of document.querySelectorAll('.seconds')) {
        horloge.style.transform = 'rotate(' + angleS + 'deg)';
    }
    // récupération et affichage de l'heure de mexico
    let mexicoH = curHour + 12 - 7
    let angleMH = mexicoH % 12 / 12 * 360
    document.querySelector('#mexicoClock .hour').style.transform = 'rotate(' + angleMH + 'deg)';
    // récupération et affichage de l'heure de l'irlande
    let IrlandH = curHour + 12 - 1
    let angleIH = IrlandH % 12 / 12 * 360
    document.querySelector('#irelandClock .hour').style.transform = 'rotate(' + angleIH + 'deg)';
    // récupération et affichage de l'heure du laos
    let LaosH = curHour + 12 + 5
    let angleLH = LaosH % 12 / 12 * 360
    document.querySelector('#laosClock .hour').style.transform = 'rotate(' + angleLH + 'deg)';
}, 1000);

for (const clockDiv of document.querySelectorAll('.littleClock h3')) {
    clockDiv.addEventListener('click', function (e) {
        e.target.nextElementSibling.classList.toggle('hidden');
    })
}
